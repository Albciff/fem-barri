//
//  FemBarriAppController.h
//  FemBarri
//
//  Created by Oscar Burgos on 25/10/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <MapKit/MapKit.h>

@class RootViewController;

@interface AppController : NSObject <UIAccelerometerDelegate, UIAlertViewDelegate, UITextFieldDelegate, UIApplicationDelegate, MKMapViewDelegate> {
    UIWindow *window;
    RootViewController    *viewController;
}

- (void) showMaps;
- (void) removeMaps;
- (void) requestCommercePositions;
- (void) plotCommercePositions:(NSString *)responseData;

@property (nonatomic, retain) UIWindow *window;
@property (nonatomic, retain) RootViewController *viewController;

@end

