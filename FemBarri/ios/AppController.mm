//
//  FemBarriAppController.mm
//  FemBarri
//
//  Created by Oscar Burgos on 25/10/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "AppController.h"
#import "cocos2d.h"
#import "EAGLView.h"
#import "AppDelegate.h"

#import "RootViewController.h"
#import <MapKit/MapKit.h>
#import "MyLocation.h"
#import "ASIHTTPRequest.h"
#import "MBProgressHUD.h"
#import "SBJSON.h"

@implementation AppController

@synthesize window;
@synthesize viewController;

#pragma mark -
#pragma mark Application lifecycle

// cocos2d application instance
static AppDelegate s_sharedApplication;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    // Override point for customization after application launch.

    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    EAGLView *__glView = [EAGLView viewWithFrame: [window bounds]
                                     pixelFormat: kEAGLColorFormatRGBA8
                                     depthFormat: GL_DEPTH_COMPONENT16
                              preserveBackbuffer: NO
                                      sharegroup: nil
                                   multiSampling: NO
                                 numberOfSamples:0 ];

    // Use RootViewController manage EAGLView
    viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    viewController.wantsFullScreenLayout = YES;
    viewController.view = __glView;
    //[viewController.view addSubview:__glView];

    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: viewController.view];
    }
    else
    {
        // use this method on ios6
        [window setRootViewController:viewController];
    }
    
    [window makeKeyAndVisible];

    [[UIApplication sharedApplication] setStatusBarHidden: YES];

    cocos2d::CCApplication::sharedApplication()->run();
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    cocos2d::CCDirector::sharedDirector()->pause();
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    cocos2d::CCDirector::sharedDirector()->resume();
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    cocos2d::CCApplication::sharedApplication()->applicationDidEnterBackground();
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    cocos2d::CCApplication::sharedApplication()->applicationWillEnterForeground();
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
     cocos2d::CCDirector::sharedDirector()->purgeCachedData();
}


- (void)dealloc {
    [super dealloc];
}

MKMapView* mMap = NULL;

- (void) showMaps
{
    #define METERS_PER_MILE 1609.344

    if (mMap == NULL) {
        cocos2d::CCSize size = cocos2d::CCDirector::sharedDirector()->getWinSize();
        mMap = [[[MKMapView alloc] initWithFrame:CGRectMake(0, size.height*0.09f, size.width, size.height*0.8f)] autorelease];
        mMap.showsUserLocation = YES;
        mMap.userTrackingMode = MKUserTrackingModeFollow;
        mMap.delegate = self;

        EAGLView *view = [EAGLView sharedEGLView];
        [view addSubview:mMap];
        //UIView* pView = [view superview];
        //[pView addSubview:mMap];

        //[pView sendSubviewToBack:mMap];
        //[window bringSubviewToFront:view];

        CLLocationCoordinate2D zoomLocation;
        zoomLocation = mMap.userLocation.location.coordinate;

        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
        
        [mMap setRegion:viewRegion animated:YES];
        [self requestCommercePositions];
    }
    else
    {
        [self removeMaps];
    }
}

- (void) removeMaps
{
    [mMap removeFromSuperview];
    mMap = NULL;
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    static NSString *identifier = @"MyLocation";
    if ([annotation isKindOfClass:[MyLocation class]]) {

        MKAnnotationView *annotationView = (MKAnnotationView *) [mMap dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            annotationView.enabled = YES;
            annotationView.canShowCallout = YES;
            //annotationView.image = [UIImage imageNamed:@"closeNormal.png"];//here we use a nice image instead of the default pins

            // Add to mapView:viewForAnnotation: after setting the image on the annotation view
            annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];

        } else {
            annotationView.annotation = annotation;
        }

        return annotationView;
    }

    return nil;
}

// IOS6 Maps - redirigeix a l'app de mapes
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    MyLocation *location = (MyLocation*)view.annotation;

    NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
    [location.mapItem openInMapsWithLaunchOptions:launchOptions];
}

// Pinta els comerços al mapa
- (void)plotCommercePositions:(NSString *)responseString {
    for (id<MKAnnotation> annotation in mMap.annotations) {
        [mMap removeAnnotation:annotation];
    }

    NSDictionary * root = [responseString JSONValue];
    NSArray *data = [root objectForKey:@"data"];

    for (NSString *row in data) {

        NSString* commerceID = row;

        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://finappsapi.bdigital.org/api/2012/33e34fbe0f/93f-a143-ec6a47d0c24c/operations/commerce/%@", commerceID]];

        // 4
        ASIHTTPRequest *_request = [ASIHTTPRequest requestWithURL:url];
        ASIHTTPRequest *request = _request;

        request.requestMethod = @"GET";

        // 5
        [request setDelegate:self];
        [request setCompletionBlock:^{
            NSString *responseString = [request responseString];
            NSLog(@"Response: %@", responseString);

            NSDictionary * root = [responseString JSONValue];
            NSDictionary *data = [root objectForKey:@"data"];

            NSArray* location = [data objectForKey:@"location"];
            NSNumber *latitude = location[0];
            NSNumber *longitude = location[1];

            NSString *commerceDescription = [root valueForKey:@"publicName"];
            NSString *address = [root valueForKey:@"address"];


            CLLocationCoordinate2D coordinate;
            coordinate.latitude = latitude.doubleValue;
            coordinate.longitude = longitude.doubleValue;
            MyLocation *annotation = [[MyLocation alloc] initWithName:commerceDescription address:address coordinate:coordinate] ;
            [mMap addAnnotation:annotation];

        }];
        [request setFailedBlock:^{
            [MBProgressHUD hideHUDForView:mMap.superview animated:YES];
            NSError *error = [request error];
            NSLog(@"Error: %@", error.localizedDescription);
        }];
        
        // 6
        [request startAsynchronous];
	}

    [MBProgressHUD hideHUDForView:mMap.superview animated:YES];
}

- (void) requestCommercePositions{
    // 1
    MKCoordinateRegion mapRegion = [mMap region];
    CLLocationCoordinate2D centerLocation = mapRegion.center;

    centerLocation.longitude = 2.169921;
    centerLocation.latitude = 41.387956;

    // 2
//    NSString *jsonFile = [[NSBundle mainBundle] pathForResource:@"command" ofType:@"json"];
//    NSString *formatString = [NSString stringWithContentsOfFile:jsonFile encoding:NSUTF8StringEncoding error:nil];
//    NSString *json = [NSString stringWithFormat:formatString,
//                      centerLocation.latitude, centerLocation.longitude, 0.5*METERS_PER_MILE];

    // 3
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://finappsapi.bdigital.org/api/2012/33e34fbe0f/93f-a143-ec6a47d0c24c/operations/commerce/search/near?lat=%f&lng=%f&radius=1.0", (double)centerLocation.latitude, (double)centerLocation.longitude]];

    // 4
    ASIHTTPRequest *_request = [ASIHTTPRequest requestWithURL:url];
    //__weak ASIHTTPRequest *request = _request;
    ASIHTTPRequest *request = _request;

    request.requestMethod = @"GET";
    //[request addRequestHeader:@"Content-Type" value:@"application/json"];
    //[request appendPostData:[json dataUsingEncoding:NSUTF8StringEncoding]];

    // 5
    [request setDelegate:self];
    [request setCompletionBlock:^{
        NSString *responseString = [request responseString];
        NSLog(@"Response: %@", responseString);
        [self plotCommercePositions:responseString];
    }];
    [request setFailedBlock:^{
        [MBProgressHUD hideHUDForView:mMap.superview animated:YES];
        NSError *error = [request error];
        NSLog(@"Error: %@", error.localizedDescription);
    }];

    // 6
    [request startAsynchronous];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:mMap.superview animated:YES];
    hud.labelText = @"Carregant ofertes...";
}

@end