#ifndef __CHECKOUT_SCENE_H__
#define __CHECKOUT_SCENE_H__

#include "cocos2d.h"

class Checkout : public cocos2d::CCLayer
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();

    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::CCScene* scene();

    void menuBackCallback(CCObject* pSender);

    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(Checkout);
};

#endif // __CHECKOUT_SCENE_H__
