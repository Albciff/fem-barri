#ifndef __OFFERDETAILS_SCENE_H__
#define __OFFERDETAILS_SCENE_H__

#include "cocos2d.h"
#include "../Mocks/ProductPromotion.h"

using namespace cocos2d;

class OfferDetails : public cocos2d::CCLayer
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();

    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::CCScene* scene(ProductPromotion* promotion, int step);
    
    void fillWithProductPromotion(ProductPromotion* promotion, int step);

    // a selector callback
    void menuBackCallback(CCObject* pSender);

    void menuCheckoutCallback(CCObject* pSender);

    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(OfferDetails);
    
    void backtoscreen1();
    void backtoscreen2();
    void showMap();
    void hideMap();

	int count;
    int step;
    CCMenuItemImage* bBackMap;
    CCMenuItemImage *pCheckout;
};

#endif // __OFFERDETAILS_SCENE_H__
