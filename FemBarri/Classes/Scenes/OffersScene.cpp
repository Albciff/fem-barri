#include "OffersScene.h"
#include "OfferDetailsScene.h"
#include "GameProgressScene.h"

#include "SimpleAudioEngine.h"
#include "Constants.h"

#include <iostream>

#include "../Services/MockProductService.h"
#include "../NativeCallsEngine/include/NativeCalls.h"

#include "../extensions/GUI/CCScrollView/CCTableView.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace NativeCallsEngine;
using namespace cocos2d::extension;

using namespace std;

CCScene* Offers::scene(int step)
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();

    // 'layer' is an autorelease object
    Offers *layer = Offers::create();
    layer->initialize(step);
    // add layer as a child to scene
    scene->addChild(layer);
    
    

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Offers::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    
    
    return true;
}

void Offers::initialize(int step){
    this->step = step;
    this->products = new MockProductService();
    
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    
    CCSprite* sprite = CCSprite::create("pagina2.png");
    sprite->setPosition(ccp(size.width/2, size.height/2));
    
    this->addChild(sprite);
    
    CCTableView* tableView = CCTableView::create(this, CCSizeMake(321, 240));
	tableView->setDirection(kCCScrollViewDirectionVertical);
	tableView->setPosition(ccp(size.width/2-321/2, size.height/2-234/2));
    
	tableView->setDelegate(this);
	tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	this->addChild(tableView);
	tableView->reloadData();
    
    CCMenuItemImage* bBack = CCMenuItemImage::create("bototornara.png", "bototornarb.png", this, menu_selector(Offers::menuBackCallback));
    bBack->setPosition(ccp(20+bBack->getContentSize().width*0.5f, 420));
    
    CCMenu* menu = CCMenu::create(bBack, NULL);
    menu->setPosition(CCPointZero);
    addChild(menu);
    
    int i = 0;
    for(i = 0; i < this->step; i++){
        CCSprite* sprite = CCSprite::create("laCaixaLogoEstrella.png");
        sprite->setPosition(ccp(180+i*50, 43));
        
        this->addChild(sprite);
    }
    for(;i <=2;i++){
        CCSprite* sprite = CCSprite::create("laCaixaLogoEstrella2.png");
        sprite->setPosition(ccp(180+i*50, 43));
        
        this->addChild(sprite);
    }
}

void Offers::menuBackCallback(cocos2d::CCObject *pSender)
{
    CCTransitionSlideInL* transition = CCTransitionSlideInL::create(0.5f, GameProgress::scene(1));
    CCDirector::sharedDirector()->replaceScene(transition);
}

void Offers::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{
    CCLOG("cell touched at index: %i", cell->getIdx());
    
    GynkamaStep *gstep = this->products->getGynkamaStep(this->step);
    ProductPromotion *promotion;
    if (cell->getIdx() == 0){
        promotion = gstep->offer1;
    }
    if (cell->getIdx() == 1){
        promotion = gstep->offer2;
    }
    if (cell->getIdx() == 2){
        promotion = gstep->offer3;
    }
    
    CCTransitionSlideInR* transition = CCTransitionSlideInR::create(0.5f, OfferDetails::scene(promotion, this->step));
    CCDirector::sharedDirector()->pushScene(transition);
}

CCSize Offers::cellSizeForTable(CCTableView *table)
{
    return CCSizeMake(280, 80);
}

CCTableViewCell* Offers::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
    GynkamaStep *gstep = this->products->getGynkamaStep(this->step);
    CCTableViewCell *cell = table->dequeueCell();
    if (!cell) {
        
        
        
        cell = new CCTableViewCell();
        cell->autorelease();
        
        CCSprite *sprite;
        
        if(idx == 0){
            sprite = CCSprite::create(gstep->offer1->product.imagePath.c_str());
        }
        if(idx == 1){
            sprite = CCSprite::create(gstep->offer2->product.imagePath.c_str());
        }
        if(idx == 2){
            sprite = CCSprite::create(gstep->offer3->product.imagePath.c_str());
        }
        
        
        sprite->setAnchorPoint(CCPointZero);
        sprite->setPosition(ccp(0, 0));
        cell->addChild(sprite);
 
    }
    else
    {
        
    }


    return cell;
}

unsigned int Offers::numberOfCellsInTableView(CCTableView *table)
{
    return 3;
}