#ifndef __GAMEMODES_SCENE_H__
#define __GAMEMODES_SCENE_H__

#include "cocos2d.h"
#include "cocos-ext.h"

class GameModes : public cocos2d::CCLayer
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();

    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::CCScene* scene(int mode);

    void setMode(int mode);

    // a selector callback
    void menuBackCallback(CCObject* pSender);

    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(GameModes);

    void changeMenuItem(CCNode* menuItem, void* data);
    void flipSpriteX(cocos2d::CCSprite* in, cocos2d::CCSprite* out, cocos2d::tOrientation orientation, float m_fDuration);
    void finishFlipX();

    void menu0Callback(CCObject* pSender);
    void menu1Callback(CCObject* pSender);
    void menu2Callback(CCObject* pSender);
    void menu3Callback(CCObject* pSender);
    void menu4Callback(CCObject* pSender);
    void menu5Callback(CCObject* pSender);

protected:
    int currentMode;

    cocos2d::CCSprite* pic, *pic2;
    cocos2d::CCMenuItemImage* mItem1, * mItem2, * mItem3, * mItem4, * mItem5;
    cocos2d::CCLabelTTF* label;
};

#endif // __OFFERDETAILS_SCENE_H__
