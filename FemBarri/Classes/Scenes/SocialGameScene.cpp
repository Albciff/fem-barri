#include "SocialGameScene.h"
#include "SimpleAudioEngine.h"

#include "../NativeCallsEngine/include/NativeCalls.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace NativeCallsEngine;

CCScene* SocialGame::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();

    // 'layer' is an autorelease object
    SocialGame *layer = SocialGame::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool SocialGame::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }

    

    return true;
}

void SocialGame::menuBackCallback(cocos2d::CCObject *pSender)
{

}