#include "GameModesScene.h"
#include "SimpleAudioEngine.h"
#include "MainScene.h"
#include "GameProgressScene.h"

#include "Constants.h"

#include "CCTableView.h"

#include "../NativeCallsEngine/include/NativeCalls.h"
#include "../Services/MockProductService.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace NativeCallsEngine;
using namespace cocos2d::extension;

CCScene* GameModes::scene(int mode)
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();

    // 'layer' is an autorelease object
    GameModes *layer = GameModes::create();
    layer->setMode(mode);

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameModes::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }

    return true;
}

void GameModes::setMode(int mode)
{
    currentMode = mode;

    CCSprite* bg;
    CCMenuItemImage* mItem0, * mBack;

    if (currentMode == 0) {
        // ética
        pic = CCSprite::create("cercle_gran_etica.png");
        bg = CCSprite::create("pagina.png");

        mItem1 = CCMenuItemImage::create("blau_petit-1.png", "blau_petit-1.png", this, menu_selector(GameModes::menu1Callback));
        mItem2 = CCMenuItemImage::create("blau_petit-2.png", "blau_petit-2.png", this, menu_selector(GameModes::menu2Callback));
        mItem3 = CCMenuItemImage::create("blau_petit-3.png", "blau_petit-3.png", this, menu_selector(GameModes::menu3Callback));
        mItem4 = CCMenuItemImage::create("blau_petit-4.png", "blau_petit-4.png", this, menu_selector(GameModes::menu4Callback));
        mItem5 = CCMenuItemImage::create("blau_petit-5.png", "blau_petit-5.png", this, menu_selector(GameModes::menu5Callback));

        label = CCLabelTTF::create("Criteris ètics", fontName2.c_str(), 20);
    }
    else
    {
        // Productes cercle gran_productes
        pic = CCSprite::create("cercle_gran_productes.png");
        bg = CCSprite::create("pagina.png");

        mItem1 = CCMenuItemImage::create("blau_petit-6.png", "blau_petit-6.png", this, menu_selector(GameModes::menu1Callback));
        mItem2 = CCMenuItemImage::create("blau_petit-7.png", "blau_petit-7.png", this, menu_selector(GameModes::menu2Callback));
        mItem3 = CCMenuItemImage::create("blau_petit-8.png", "blau_petit-8.png", this, menu_selector(GameModes::menu3Callback));
        mItem4 = CCMenuItemImage::create("blau_petit-9.png", "blau_petit-9.png", this, menu_selector(GameModes::menu4Callback));
        mItem5 = CCMenuItemImage::create("blau_petit-10.png", "blau_petit-10.png", this, menu_selector(GameModes::menu5Callback));

        label = CCLabelTTF::create("Productes", fontName2.c_str(), 20);
    }

    mBack = CCMenuItemImage::create("bototornara.png", "bototornarb.png", this, menu_selector(GameModes::menuBackCallback));
    mBack->setPosition(ccp(20+mBack->getContentSize().width*0.5f, 420));

    mItem0 = CCMenuItemImage::create("botogirara.png", "botogirarb.png", this, menu_selector(GameModes::menu0Callback));
    mItem0->setPosition(ccp(300-mBack->getContentSize().width*0.5f, 420));

    pic->setPosition(ccp(160, 293));
    addChild(pic);

    bg->setPosition(ccp(160, 240));
    addChild(bg);

    mItem1->setPosition(ccp(78, 73));
    mItem2->setPosition(ccp(119, 73));
    mItem3->setPosition(ccp(160, 73));
    mItem4->setPosition(ccp(201, 73));
    mItem5->setPosition(ccp(242, 73));

    label->setPosition(ccp(300-label->getContentSize().width*0.5f, 460));
    label->setColor(ccBLACK);
    addChild(label);

    CCMenu* menu = CCMenu::create(mItem1, mItem2, mItem3, mItem4, mItem5, mBack, mItem0, NULL);
    menu->setPosition(CCPointZero);
    addChild(menu);

    CCSprite* caixa = CCSprite::create("logopetit.png");
    caixa->setPosition(ccp(53, 460));
    addChild(caixa);

}

void GameModes::menuBackCallback(cocos2d::CCObject *pSender)
{
    CCTransitionSlideInL* transition = CCTransitionSlideInL::create(0.5f, Main::scene());
	CCDirector::sharedDirector()->replaceScene(transition);
}

void GameModes::menu0Callback(cocos2d::CCObject *pSender)
{
    int newMode = (currentMode == 0) ? 1 : 0;

    pic2 = CCSprite::create((newMode == 0) ? "cercle_gran_etica.png" : "cercle_gran_productes.png");
    pic2->setPosition(ccp(160, 293));
    addChild(pic2);

    currentMode = newMode;
    flipSpriteX(pic2, pic, kOrientationRightOver, 0.8f);

    mItem1->runAction(CCSequence::create(CCFadeOut::create(0.4f), CCCallFuncND::create(this, callfuncND_selector(GameModes::changeMenuItem), (void*)1), CCFadeIn::create(0.4f), NULL));
    mItem2->runAction(CCSequence::create(CCFadeOut::create(0.4f), CCCallFuncND::create(this, callfuncND_selector(GameModes::changeMenuItem), (void*)2), CCFadeIn::create(0.4f), NULL));
    mItem3->runAction(CCSequence::create(CCFadeOut::create(0.4f), CCCallFuncND::create(this, callfuncND_selector(GameModes::changeMenuItem), (void*)3), CCFadeIn::create(0.4f), NULL));
    mItem4->runAction(CCSequence::create(CCFadeOut::create(0.4f), CCCallFuncND::create(this, callfuncND_selector(GameModes::changeMenuItem), (void*)4), CCFadeIn::create(0.4f), NULL));
    mItem5->runAction(CCSequence::create(CCFadeOut::create(0.4f), CCCallFuncND::create(this, callfuncND_selector(GameModes::changeMenuItem), (void*)5), CCFadeIn::create(0.4f), NULL));
    label->runAction(CCSequence::create(CCFadeOut::create(0.4f), CCCallFuncND::create(this, callfuncND_selector(GameModes::changeMenuItem), (void*)0), CCFadeIn::create(0.4f), NULL));
    
}

void GameModes::changeMenuItem(CCNode* menuItem, void* data)
{
    int menuItemID = (int) data;

    switch (menuItemID) {
        case 1:
            mItem1->setNormalImage(CCSprite::create((currentMode == 0) ? "blau_petit-1.png" : "blau_petit-6.png"));
            mItem1->setSelectedImage(CCSprite::create((currentMode == 0) ? "blau_petit-1.png" : "blau_petit-6.png"));
            break;

        case 2:
            mItem2->setNormalImage(CCSprite::create((currentMode == 0) ? "blau_petit-2.png" : "blau_petit-7.png"));
            mItem2->setSelectedImage(CCSprite::create((currentMode == 0) ? "blau_petit-2.png" : "blau_petit-7.png"));
            break;

        case 3:
            mItem3->setNormalImage(CCSprite::create((currentMode == 0) ? "blau_petit-3.png" : "blau_petit-8.png"));
            mItem3->setSelectedImage(CCSprite::create((currentMode == 0) ? "blau_petit-3.png" : "blau_petit-8.png"));
            break;

        case 4:
            mItem4->setNormalImage(CCSprite::create((currentMode == 0) ? "blau_petit-4.png" : "blau_petit-9.png"));
            mItem4->setSelectedImage(CCSprite::create((currentMode == 0) ? "blau_petit-4.png" : "blau_petit-9.png"));
            break;

        case 5:
            mItem5->setNormalImage(CCSprite::create((currentMode == 0) ? "blau_petit-5.png" : "blau_petit-10.png"));
            mItem5->setSelectedImage(CCSprite::create((currentMode == 0) ? "blau_petit-5.png" : "blau_petit-10.png"));
            break;
        case 0:
            label->setString((currentMode == 0) ? "Criteris ètics" : "Productes");
            break;
    }

    
}

void GameModes::flipSpriteX(CCSprite* in, CCSprite* out, tOrientation orientation, float m_fDuration)
{
    CCActionInterval *inA, *outA;
    in->setVisible(false);

    float inDeltaZ, inAngleZ;
    float outDeltaZ, outAngleZ;

    if( orientation == kOrientationRightOver )
    {
        inDeltaZ = 90;
        inAngleZ = 270;
        outDeltaZ = 90;
        outAngleZ = 0;
    }
    else
    {
        inDeltaZ = -90;
        inAngleZ = 90;
        outDeltaZ = -90;
        outAngleZ = 0;
    }

    inA = (CCActionInterval*)CCSequence::create
    (
     CCDelayTime::create(m_fDuration/2),
     CCShow::create(),
     CCOrbitCamera::create(m_fDuration/2, 1, 0, inAngleZ, inDeltaZ, 0, 0),
     CCCallFunc::create(this, callfunc_selector(GameModes::finishFlipX)),
     NULL
     );

    outA = (CCActionInterval *)CCSequence::create
    (
     CCOrbitCamera::create(m_fDuration/2, 1, 0, outAngleZ, outDeltaZ, 0, 0),
     CCHide::create(),
     CCDelayTime::create(m_fDuration/2),
     NULL
     );

    in->runAction(inA);
    out->runAction(outA);
}

void GameModes::finishFlipX()
{
    // clean up
    CCSprite* final = pic2;
    pic->removeFromParentAndCleanup(true);
    pic = final;
    pic2 = NULL;
}

void GameModes::menu1Callback(cocos2d::CCObject *pSender)
{
    CCTransitionSlideInR* transition;
    
    if (currentMode == 0) {
        // ética
        transition = CCTransitionSlideInR::create(0.5f, GameProgress::scene(1));
    }
    else{
        transition = CCTransitionSlideInR::create(0.5f, GameProgress::scene(6));
    }

    CCDirector::sharedDirector()->replaceScene(transition);
}

void GameModes::menu2Callback(cocos2d::CCObject *pSender)
{
    CCTransitionSlideInR* transition;

    if (currentMode == 0) {
        // ética
        transition = CCTransitionSlideInR::create(0.5f, GameProgress::scene(2));
    }
    else{
        transition = CCTransitionSlideInR::create(0.5f, GameProgress::scene(7));
    }

    CCDirector::sharedDirector()->replaceScene(transition);
}

void GameModes::menu3Callback(cocos2d::CCObject *pSender)
{
    CCTransitionSlideInR* transition;

    if (currentMode == 0) {
        // ética
        transition = CCTransitionSlideInR::create(0.5f, GameProgress::scene(3));
    }
    else{
        transition = CCTransitionSlideInR::create(0.5f, GameProgress::scene(8));
    }

    CCDirector::sharedDirector()->replaceScene(transition);
}

void GameModes::menu4Callback(cocos2d::CCObject *pSender)
{
    CCTransitionSlideInR* transition;

    if (currentMode == 0) {
        // ética
        transition = CCTransitionSlideInR::create(0.5f, GameProgress::scene(4));
    }
    else{
        transition = transition = CCTransitionSlideInR::create(0.5f, GameProgress::scene(9));
    }

    CCDirector::sharedDirector()->replaceScene(transition);
}

void GameModes::menu5Callback(cocos2d::CCObject *pSender)
{
    CCTransitionSlideInR* transition;

    if (currentMode == 0) {
        // ética
        transition = CCTransitionSlideInR::create(0.5f, GameProgress::scene(5));
    }
    else{
        transition = CCTransitionSlideInR::create(0.5f, GameProgress::scene(10));
    }

    CCDirector::sharedDirector()->replaceScene(transition);
}