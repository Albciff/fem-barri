#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "MainScene.h"
#include "OfferDetailsScene.h"

#include "Constants.h"

#include "CCTableView.h"

#include "../NativeCallsEngine/include/NativeCalls.h"
#include "../Services/MockProductService.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace NativeCallsEngine;
using namespace cocos2d::extension;

CCScene* Game::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();

    // 'layer' is an autorelease object
    Game *layer = Game::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Game::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }

    CCSize size = CCDirector::sharedDirector()->getWinSize();

    MockProductService* productDetails = new MockProductService();
    GynkamaStep* gym = productDetails->getGynkamaStep(0);

    promotions = gym->getOffers();

    
    CCLayer* fondo = CCLayer::create();
    CCSprite* background = CCSprite::create("Default.png");
    background->setPosition(ccp(size.width/2, size.height/2));
    fondo->addChild(background);
    this->addChild(fondo);

    

    CCTableView* tableView = CCTableView::create(this, CCSizeMake(288, 320));
	tableView->setDirection(kCCScrollViewDirectionVertical);
	tableView->setPosition(ccp(size.width*0.5f - 144, size.height*0.5f-160));
	tableView->setDelegate(this);
	tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	this->addChild(tableView);
	tableView->reloadData();
    


    CCMenuItemImage* bBack = CCMenuItemImage::create("CloseNormal.png", "CloseSelected.png", this, menu_selector(Game::menuBackCallback));
    bBack->setPosition(ccp(size.width*0.1f, size.height*0.1f));

    CCMenu* menu = CCMenu::create(bBack, NULL);
    menu->setPosition(CCPointZero);
    addChild(menu);

    return true;
}

void Game::menuBackCallback(cocos2d::CCObject *pSender)
{
    CCTransitionSlideInL* transition = CCTransitionSlideInL::create(0.5f, Main::scene());
	CCDirector::sharedDirector()->replaceScene(transition);
}

void Game::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{
    CCLOG("cell touched at index: %i", cell->getIdx());
    CCTransitionSlideInR* transition = CCTransitionSlideInR::create(0.5f, OfferDetails::scene((ProductPromotion*)promotions->objectAtIndex(cell->getIdx()), 0));
    CCDirector::sharedDirector()->pushScene(transition);
}

CCSize Game::cellSizeForTable(CCTableView *table)
{
    return CCSizeMake(280, 80);
}

CCTableViewCell* Game::tableCellAtIndex(CCTableView *table, unsigned int idx)
{

#define cellImage 123
#define cellLabel 124
#define cellDescr 125

    CCTableViewCell *cell = table->dequeueCell();
    if (!cell) {
        cell = new CCTableViewCell();
        cell->autorelease();

        // Image fons celda
        CCSprite *cellBG = CCSprite::create("Icon.png");
        cellBG->setAnchorPoint(CCPointZero);
        cellBG->setPosition(ccp(140, 40));
        cell->addChild(cellBG);

        // Imatge oferta
        CCSprite *offerIMG = CCSprite::create(((ProductPromotion*)promotions->objectAtIndex(idx))->product.imagePath.c_str());
        offerIMG->setAnchorPoint(CCPointZero);
        offerIMG->setPosition(ccp(20, 40));
        offerIMG->setTag(cellImage);
        cell->addChild(offerIMG);

        // Text oferta
        CCLabelTTF *label = CCLabelTTF::create(((ProductPromotion*)promotions->objectAtIndex(idx))->product.name.c_str(), fontName.c_str(), 20.0);
        label->setPosition(CCPointZero);
        label->setAnchorPoint(CCPointZero);
        label->setTag(cellLabel);
        cell->addChild(label);

        // Descr oferta
        CCLabelTTF *desc = CCLabelTTF::create(((ProductPromotion*)promotions->objectAtIndex(idx))->product.description.c_str(), fontName.c_str(), 20.0);
        desc->setPosition(CCPointZero);
        desc->setAnchorPoint(CCPointZero);
        desc->setTag(cellDescr);
        cell->addChild(desc);
    }
    else
    {
//        CCSprite* offerIMG = (CCSprite*) cell->getChildByTag(cellImage);
//        if (!offerIMG) {
//            // Retry carregar
//            offerIMG = CCSprite::create(((ProductPromotion*)promotions->objectAtIndex(idx))->product.imagePath.c_str());
//            if (offerIMG) {
//                offerIMG->setAnchorPoint(CCPointZero);
//                offerIMG->setPosition(ccp(20, 40));
//                offerIMG->setTag(cellImage);
//                cell->addChild(offerIMG);
//            }
//        }
//
//        CCLabelTTF *label = (CCLabelTTF*)cell->getChildByTag(cellLabel);
//        if (!label) {
//            // Retry carregar
//            label = CCLabelTTF::create(((ProductPromotion*)promotions->objectAtIndex(idx))->product.name.c_str(), fontName.c_str(), 20.0);
//            if (label) {
//                label->setPosition(CCPointZero);
//                label->setAnchorPoint(CCPointZero);
//                label->setTag(cellLabel);
//                cell->addChild(label);
//            }
//        }
//
//        CCLabelTTF *desc = (CCLabelTTF*)cell->getChildByTag(cellDescr);
//        if(!desc) {
//            // Retry carregar
//            desc = CCLabelTTF::create(((ProductPromotion*)promotions->objectAtIndex(idx))->product.name.c_str(), fontName.c_str(), 20.0);
//            if (desc) {
//                desc->setPosition(CCPointZero);
//                desc->setAnchorPoint(CCPointZero);
//                desc->setTag(cellDescr);
//                cell->addChild(desc);
//            }
//        }
    }


    return cell;
}

unsigned int Game::numberOfCellsInTableView(CCTableView *table)
{
    return 3;
}