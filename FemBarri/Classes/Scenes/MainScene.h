#ifndef __MAIN_SCENE_H__
#define __MAIN_SCENE_H__

#include "cocos2d.h"

#include "../Services/MockClientService.h"

class Main : public cocos2d::CCLayer
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();

    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::CCScene* scene();

    // Scene's Buttons callbacks
    void menuProfileCallback(CCObject* pSender);
    void menuMapCallback(CCObject* pSender);
    void menuGameCallback(CCObject* pSender);
    void menuSocialGameCallback(CCObject* pSender);

    MockClientService* client;
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(Main);

protected:
    CCMenuItemImage* backMap;
};

#endif // __MAIN_SCENE_H__
