#include "SplashScene.h"
#include "SimpleAudioEngine.h"
#include "MainScene.h"
#include "Constants.h"

#include "../NativeCallsEngine/include/NativeCalls.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace NativeCallsEngine;

CCScene* Splash::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();

    // 'layer' is an autorelease object
    Splash *layer = Splash::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Splash::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }

    CCSize size = CCDirector::sharedDirector()->getWinSize();

    CCSprite* bg = CCSprite::create("Default.png");
	bg->setPosition(ccp(size.width * 0.5f, size.height * 0.5f));
	this->addChild(bg, 0);

	this->schedule(schedule_selector(Splash::goToOtherSplashScreen), 3.0f);

    return true;
}

void Splash::goToOtherSplashScreen(float dt)
{
	this->unschedule(schedule_selector(Splash::goToOtherSplashScreen));

	CCSize size = CCDirector::sharedDirector()->getWinSize();

    CCLayerColor *b1 = CCLayerColor::create(ccc4(255,255,255,255), size.width, size.height);
	this->addChild(b1, 0);

	// load and display your first logo
	CCSprite* bg = CCSprite::create("finappsLogo.png");
	bg->setPosition(ccp(size.width * 0.5f, size.height * 0.65f));
	this->addChild(bg, 0);

    CCSprite* bg3 = CCSprite::create("laCaixaLogo.png");
	bg3->setPosition(ccp(size.width * 0.5f, size.height * 0.35f));
	this->addChild(bg3, 0);
    bg3->setOpacity(0);
    bg3->runAction(CCSequence::create(CCDelayTime::create(0.3f), CCFadeIn::create(0.3f), CCDelayTime::create(3.0f), CCCallFunc::create(this, callfunc_selector(Splash::goToMainMenu)), NULL));
}

void Splash::goToMainMenu()
{
	CCScene *scene = Main::scene();

	CCTransitionFade* transition = CCTransitionFade::create(0.5f, scene, ccBLACK);

	CCDirector::sharedDirector()->replaceScene(transition);
}

void Splash::removeCCNodeAndCleanUp(CCNode* sender, void* data)
{
	CCSprite* img = (CCSprite*) data;
	img->removeFromParentAndCleanup(true);
}