#include "OfferDetailsScene.h"
#include "OffersScene.h"
#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "Constants.h"
#include "MainScene.h"
#include "LogrosScene.h"

#include "../NativeCallsEngine/include/NativeCalls.h"
#include "../Services/MockProductService.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace NativeCallsEngine;

CCScene* OfferDetails::scene(ProductPromotion* promotion, int step)
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();

    // 'layer' is an autorelease object
    OfferDetails *layer = OfferDetails::create();
    layer->fillWithProductPromotion(promotion, step);

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool OfferDetails::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }

    return true;
}

void OfferDetails::fillWithProductPromotion(ProductPromotion* promotion, int step){
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    
    this->step = step;
    
    Product product = promotion->product;
    
    CCLabelTTF* productName = CCLabelTTF::create(product.name.c_str(), fontName.c_str(), 24);
    productName->setPosition(ccp(size.width*0.5f, size.height*0.9f));
    addChild(productName);
    
    CCLabelTTF* productDesc = CCLabelTTF::create(product.description.c_str(), fontName.c_str(), 24);
    productDesc->setPosition(ccp(size.width*0.5f, size.height*0.7f));
    addChild(productDesc);
    
    CCLabelTTF* productPrice = CCLabelTTF::create(CCString::createWithFormat("%.2f", product.price)->getCString(), fontName.c_str(), 24);
    productPrice->setPosition(ccp(size.width*0.5f, size.height*0.5f));
    addChild(productPrice);
    
    CCLabelTTF* productImgPath = CCLabelTTF::create(product.imagePath.c_str(), fontName.c_str(), 24);
    productImgPath->setPosition(ccp(size.width*0.5f, size.height*0.3f));
    addChild(productImgPath);
    
    
    CCSprite* sprite = CCSprite::create(promotion->detailPath.c_str());
    sprite->setPosition(ccp(size.width/2, size.height/2));
    
    this->addChild(sprite);

    CCSprite*
    icon = CCSprite::create("blau_petit-1.png");
    icon->setPosition(ccp(300-icon->getContentSize().width*0.5f, 420));
    addChild(icon);

    
    CCMenuItemImage* bBack = CCMenuItemImage::create("bototornara.png", "bototornarb.png", this, menu_selector(OfferDetails::menuBackCallback));
    bBack->setPosition(ccp(20+bBack->getContentSize().width*0.5f, 420));

    pCheckout = CCMenuItemImage::create("botocomprara.png", "botocomprarb.png", this, menu_selector(OfferDetails::menuCheckoutCallback));
    pCheckout->setPosition(ccp(20+bBack->getContentSize().width*0.5f, 95));
    
    CCMenuItemImage *pMap = CCMenuItemImage::create("botogeoa.png", "botogeob.png", this, menu_selector(OfferDetails::showMap));
    pMap->setPosition(ccp(20+bBack->getContentSize().width*0.5f, 160));
    
    bBackMap = CCMenuItemImage::create("bototornara.png", "bototornarb.png", this, menu_selector(OfferDetails::hideMap));
    bBackMap->setPosition(ccp(20 + bBackMap->getContentSize().width*0.5f, 28));
    bBackMap->setVisible(false);
	
    CCMenu* menu = CCMenu::create(bBack, pCheckout, pMap,bBackMap, NULL);
    menu->setPosition(CCPointZero);
    addChild(menu);
}

void OfferDetails::menuBackCallback(cocos2d::CCObject *pSender)
{
    CCDirector::sharedDirector()->popScene();
}

void OfferDetails::menuCheckoutCallback(cocos2d::CCObject *pSender)
{
    // transfer
    
   
    

	// loadPanel, and confirmation
	string txt = "Realitzant pagament...";
	CCLabelTTF* label = CCLabelTTF::create(txt.c_str(),fontName.c_str(),14.0f);
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    label->setColor(ccBLACK);
    label->setPosition(ccp(300 - label->getContentSize().width*0.5f, 43));

	this->addChild(label);
    
    CCFadeIn *fadein4 = CCFadeIn::create(1.0f);
    CCFadeOut *fadeout = CCFadeOut::create(1.0f);
    CCSequence *seq4 = (CCSequence*)CCSequence::create(fadeout,fadein4, fadeout,fadein4,NULL);
    

    pCheckout->runAction(seq4);

	CCSprite* creditCard = CCSprite::create("Icon.png");
	creditCard->setPosition(ccp(55, size.height-340.0f));
    creditCard->setVisible(false);
	this->addChild(creditCard);

	CCRotateTo* rotate = CCRotateTo::create(3.0f, 0.8f);
    
    CCSprite* compra = CCSprite::create("botocomprat.png");
	compra->setPosition(ccp(300 - label->getContentSize().width*0.5f, 43));
    compra->setOpacity(0);
    compra->setVisible(true);
	this->addChild(compra);
    
    
    CCRotateTo* rotate2 = CCRotateTo::create(1.0f, 0.8f);
    CCFadeIn *fadein = CCFadeIn::create(2.0f);
    CCSequence *seq1 = (CCSequence*)CCSequence::create(rotate2, fadein,NULL);
    compra->runAction(seq1);
    
    CCRotateTo* rotate3 = CCRotateTo::create(0.5f, 0.8f);
    CCFadeOut *fadein2 = CCFadeOut::create(2.0f);
    CCSequence *seq2 = (CCSequence*)CCSequence::create(rotate3, fadein2,NULL);
    label->runAction(seq2);

    
	
	//creditCard->runAction(rotate);

    if(this->step < 2){
    
        CCCallFunc* cc = CCCallFunc::create(this, callfunc_selector(OfferDetails::backtoscreen1));
        CCSequence *seq = (CCSequence*)CCSequence::create(rotate, cc,NULL);
        creditCard->runAction(seq);
	
    }else{
        CCCallFunc* cc = CCCallFunc::create(this, callfunc_selector(OfferDetails::backtoscreen2));
        CCSequence *seq = (CCSequence*)CCSequence::create(rotate, cc,NULL);
        creditCard->runAction(seq);
    }
}

void OfferDetails::backtoscreen1(){
    CCTransitionSlideInR* transition = CCTransitionSlideInR::create(0.5f, Offers::scene(this->step+1));
    CCDirector::sharedDirector()->replaceScene(transition);
}

void OfferDetails::backtoscreen2(){
    CCScene *scene = LogrosScene::scene();
    
    CCTransitionFade* transition = CCTransitionFade::create(0.5f, scene, ccBLACK);
    
    CCDirector::sharedDirector()->replaceScene(transition);
}

void OfferDetails::showMap(){
    bBackMap->setVisible(true);
    NativeCalls::sharedEngine()->showMap();
}

void OfferDetails::hideMap(){
    bBackMap->setVisible(false);
    NativeCalls::sharedEngine()->showMap();
}
