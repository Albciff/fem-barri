//
//  LogrosScene.cpp
//  FemBarri
//
//  Created by Daniel García Pérez on 27/10/12.
//
//

#include "LogrosScene.h"
#include "MainScene.h"
#include "../NativeCallsEngine/include/NativeCalls.h"

using namespace cocos2d;
using namespace NativeCallsEngine;

CCScene* LogrosScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    LogrosScene *layer = LogrosScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool LogrosScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }

    
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    
    CCSprite* sprite = CCSprite::create("logros.png");
    sprite->setPosition(ccp(size.width/2, size.height/2));

    this->addChild(sprite);
    
    CCMenuItemImage* bBack = CCMenuItemImage::create("bototornara.png", "bototornarb.png", this, menu_selector(LogrosScene::menuBackCallback));
    bBack->setPosition(ccp(30.0f, size.height-30.0f));
    
    CCMenu* menu = CCMenu::create(bBack, NULL);
    menu->setPosition(CCPointZero);
    addChild(menu);
    
    
    return true;
}

void LogrosScene::menuBackCallback(cocos2d::CCObject *pSender)
{
    CCTransitionSlideInL* transition = CCTransitionSlideInL::create(0.5f, Main::scene());
    CCDirector::sharedDirector()->replaceScene(transition);
}