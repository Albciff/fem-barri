#include "GameProgressScene.h"
#include "SimpleAudioEngine.h"
#include "GameModesScene.h"
#include "OffersScene.h"

#include "Constants.h"

#include "../NativeCallsEngine/include/NativeCalls.h"
#include "../Services/MockProductService.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace NativeCallsEngine;
using namespace cocos2d::extension;

CCScene* GameProgress::scene(int mode)
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();

    // 'layer' is an autorelease object
    GameProgress *layer = GameProgress::create();
    layer->setMode(mode);

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameProgress::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }

    return true;
}

void GameProgress::setMode(int mode)
{
    currentMode = mode;

    CCSprite* bg, *pic;
    CCMenuItemImage* mBack, *play;
    CCLabelTTF* label, *label2, *label3;

    pic = CCSprite::create(CCString::createWithFormat("cercle_gran_%d.png", currentMode)->getCString());
    pic->setPosition(ccp(160, 293));
    addChild(pic);

    bg = CCSprite::create("pagina.png");
    bg->setPosition(ccp(160, 240));
    addChild(bg);

    play = CCMenuItemImage::create(CCString::createWithFormat("blau gran-%d.png", currentMode)->getCString(), CCString::createWithFormat("blau gran-%d.png", currentMode)->getCString(), this, menu_selector(GameProgress::menu0Callback));
    play->setPosition(ccp(160, 293));
    
    mBack = CCMenuItemImage::create("bototornara.png", "bototornarb.png", this, menu_selector(GameProgress::menuBackCallback));
    mBack->setPosition(ccp(20+mBack->getContentSize().width*0.5f, 420));


    switch (currentMode) {
        case 1:
			
            label = CCLabelTTF::create("Criteris ètics", fontName2.c_str(), 20);
            label2 = CCLabelTTF::create("PRODUCTOR O ARTESÀ LOCAL", fontName3.c_str(), 14);
			label3 = CCLabelTTF::create("Compra producte de proximitat, beneficia els \nproductors locals i el medi ambient amb \nproductes de km0 i de temporada.", fontName3.c_str(), 13);
            break;

        case 2:
            label = CCLabelTTF::create("Criteris ètics", fontName2.c_str(), 20);
            label2 = CCLabelTTF::create("PRODUCTE ECOLÒGIC CERTIFICAT", fontName3.c_str(), 14);
			label3 = CCLabelTTF::create("Tria producte ecològic certificat, elimina l'ús de \n pesticides i prioritza les varietats autòctones i de \ntemporada.", fontName3.c_str(), 13);
            break;

        case 3:
            label = CCLabelTTF::create("Criteris ètics", fontName2.c_str(), 20);
            label2 = CCLabelTTF::create("DRETS INFANTILS I LABORALS GARANTITS", fontName3.c_str(), 13);
			label3 = CCLabelTTF::create("Amb productes de comerç just, garanteixes unes \ncondicions laborals i salarials dignes.", fontName3.c_str(), 13);
            break;

        case 4:
            label = CCLabelTTF::create("Criteris ètics", fontName2.c_str(), 20);
            label2 = CCLabelTTF::create("SENSE AFANY DE LUCRE", fontName3.c_str(), 14);
			label3 = CCLabelTTF::create("Comprant a empreses sense anim de lucre \ngaranteixes la integració de gent amb alguna \ndiscapacitat o amb risc d'exclusió social.", fontName3.c_str(), 13);
            break;

        case 5:
            label = CCLabelTTF::create("Criteris ètics", fontName2.c_str(), 20);
            label2 = CCLabelTTF::create("SENSE ADDITIUS NI CONSERVANTS", fontName3.c_str(), 14);
			label3 = CCLabelTTF::create("Compra producte fresc i saludable sense additius \nni conservants", fontName3.c_str(), 13);
            break;

        case 6:
            label = CCLabelTTF::create("Productes", fontName2.c_str(), 20);
            label2 = CCLabelTTF::create("ALIMENTACIÓ", fontName3.c_str(), 14);
			label3 = CCLabelTTF::create("Cuida els teus amb una bona alimentació!", fontName3.c_str(), 13);
            break;

        case 7:
            label = CCLabelTTF::create("Productes", fontName2.c_str(), 20);
            label2 = CCLabelTTF::create("VESTIT I COMPLEMENTS", fontName3.c_str(), 14);
			label3 = CCLabelTTF::create("Permet-et un caprici amb alguna peça de roba o \ncomplement!", fontName3.c_str(), 13);
            break;

        case 8:
            label = CCLabelTTF::create("Productes", fontName2.c_str(), 20);
            label2 = CCLabelTTF::create("REGALS", fontName3.c_str(), 14);
			label3 = CCLabelTTF::create("Fes un regal especial a algú que estimes!", fontName3.c_str(), 13);
            break;

        case 9:
            label = CCLabelTTF::create("Productes", fontName2.c_str(), 20);
            label2 = CCLabelTTF::create("LLAR", fontName3.c_str(), 14);
			label3 = CCLabelTTF::create("Vesteix la teva llar al teu gust amb una gran \nvarietat d'articles!", fontName3.c_str(), 13);
            break;

        case 10:
            label = CCLabelTTF::create("Productes", fontName2.c_str(), 20);
            label2 = CCLabelTTF::create("OCI I CULTURA", fontName3.c_str(), 14);
			label3 = CCLabelTTF::create("Cinema, teatres, circs, museus i moltes altres \nactivitats per a gaudir del teu oci, sol o acompanyat!", fontName3.c_str(), 13);
            break;
    }

    CCSprite* franja = CCSprite::create("franja.png");
    franja->setPosition(ccp(160, 112));
    addChild(franja);

    label->setPosition(ccp(300-label->getContentSize().width*0.5f, 460));
    label->setColor(ccBLACK);
    addChild(label);

    label2->setPosition(ccp(160, 112));
    addChild(label2);

	int x = 300;
	if(currentMode <=5)
		x = 320;

	label3->setPosition(ccp(160, 40 + label3->getContentSize().height*0.5f));
	label3->setColor(ccBLACK);
	addChild(label3);

    CCMenu* menu = CCMenu::create(mBack, play, NULL);
    menu->setPosition(CCPointZero);
    addChild(menu);

    CCSprite* caixa = CCSprite::create("logopetit.png");
    caixa->setPosition(ccp(53, 460));
    addChild(caixa);
}

void GameProgress::menuBackCallback(cocos2d::CCObject *pSender)
{
    int currentGameMode = (currentMode > 5) ? 1 : 0;

    CCTransitionSlideInL* transition = CCTransitionSlideInL::create(0.5f, GameModes::scene(currentGameMode));
	CCDirector::sharedDirector()->replaceScene(transition);
}

void GameProgress::menu0Callback(cocos2d::CCObject *pSender)
{
    CCTransitionSlideInR* transition = CCTransitionSlideInR::create(0.5f, Offers::scene(0));
    CCDirector::sharedDirector()->replaceScene(transition);
}