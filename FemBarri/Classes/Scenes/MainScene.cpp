#include "MainScene.h"
#include "SimpleAudioEngine.h"

#include "../NativeCallsEngine/include/NativeCalls.h"
#include "CheckoutScene.h"
#include "OffersScene.h"
#include "GameModesScene.h"
#include "SocialGameScene.h"
#include "GameScene.h"
#include "LogrosScene.h"

#include "Constants.h"

#include "../Services/MockCommerceService.h"
#include "../Services/MockProductService.h"
#include "../Services/MockTransferService.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace NativeCallsEngine;

CCScene* Main::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();

    // 'layer' is an autorelease object
    Main *layer = Main::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Main::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    client = new MockClientService();
    client->login("", "");

    CCSize size = CCDirector::sharedDirector()->getWinSize();

    CCSprite* bg = CCSprite::create("portada.png");
    bg->setPosition(ccp(size.width*0.5f, size.height*0.5f));
    addChild(bg);

    CCSprite* bola1 = CCSprite::create("bola1.png");
    bola1->setPosition(ccp(286, 166));
    addChild(bola1);
    bola1->runAction(CCSequence::create(CCDelayTime::create(2.0f), CCFadeOut::create(0.6f), NULL));

    CCSprite* bola2 = CCSprite::create("bola2.png");
    bola2->setPosition(ccp(235, 92));
    addChild(bola2);
    bola2->runAction(CCSequence::create(CCDelayTime::create(2.0f), CCFadeOut::create(0.6f), NULL));

    CCMenuItemImage* bGame = CCMenuItemImage::create("boto1gos.png", "boto1gos.png", this, menu_selector(Main::menuGameCallback));
    bGame->setPosition(ccp(185, 73));
    bGame->setOpacity(0);
    bGame->runAction(CCSequence::create(CCDelayTime::create(3.0f), CCFadeIn::create(0.6f), NULL));

    CCMenuItemImage* bMap = CCMenuItemImage::create("boto1geo.png", "boto1geo.png", this, menu_selector(Main::menuMapCallback));
    bMap->setPosition(ccp(135, 73));
    bMap->setOpacity(0);
    bMap->runAction(CCSequence::create(CCDelayTime::create(3.0f), CCFadeIn::create(0.6f), NULL));

    CCMenuItemImage* bProfile = CCMenuItemImage::create("boto1usuari.png", "boto1usuari.png", this, menu_selector(Main::menuProfileCallback));
    bProfile->setPosition(ccp(85, 73));
    bProfile->setOpacity(0);
    bProfile->runAction(CCSequence::create(CCDelayTime::create(3.0f), CCFadeIn::create(0.6f), NULL));

    CCMenuItemImage* bSocialGame = CCMenuItemImage::create("boto1estrella.png", "boto1estrella.png", this, menu_selector(Main::menuSocialGameCallback));
    bSocialGame->setPosition(ccp(235, 73));
    bSocialGame->setOpacity(0);
    bSocialGame->runAction(CCSequence::create(CCDelayTime::create(3.0f), CCFadeIn::create(0.6f), NULL));


    backMap = CCMenuItemImage::create("bototornara.png", "bototornarb.png", this, menu_selector(Main::menuMapCallback));
    backMap->setPosition(ccp(20 + backMap->getContentSize().width*0.5f, 28));
    backMap->setVisible(false);

    CCMenu* menu = CCMenu::create(bGame, bProfile, bSocialGame, bMap, backMap, NULL);
    menu->setPosition(CCPointZero);
    addChild(menu);

    CCSprite* face = CCSprite::create("cercleblaufoto.png");
    face->setPosition(ccp(160, 293));
    addChild(face);
    face->setOpacity(0);
    face->runAction(CCSequence::create(CCDelayTime::create(2.7f), CCFadeIn::create(0.6f), NULL));

    CCLabelTTF* label = CCLabelTTF::create("Comerç responsable", fontName3.c_str(), 18);
    label->setPosition(ccp(300-label->getContentSize().width*0.5f, 460));
    label->setColor(ccBLACK);
    label->setOpacity(0);
    label->runAction(CCSequence::create(CCDelayTime::create(2.7f), CCFadeIn::create(0.6f), NULL));
    addChild(label);

    return true;
}

void Main::menuGameCallback(cocos2d::CCObject *pSender)
{
    CCTransitionSlideInR* transition = CCTransitionSlideInR::create(0.5f, GameModes::scene(0));
	CCDirector::sharedDirector()->replaceScene(transition);
}

void Main::menuMapCallback(cocos2d::CCObject *pSender)
{
    NativeCalls::sharedEngine()->showMap();

    if (backMap->isVisible()) {
        backMap->setVisible(false);
    }
    else
    {
        backMap->setVisible(true);
    }
}

void loadCommerceCallback(Commerce* commerce){
    CCLog("Commerce Callback Data Load:  name:%s  street:%s number:%s postalCode:%s city:%s country:%s",  commerce->name.c_str(), commerce->addressStreet.c_str(), commerce->addressNumber.c_str(), commerce->addressPostalCode.c_str(), commerce->addressCity.c_str(), commerce->addressCountry.c_str());
}

void transferCallback(){
    CCLog("blablabla");
}

void Main::menuProfileCallback(cocos2d::CCObject *pSender)
{
//    CCTransitionSlideInR* transition = CCTransitionSlideInR::create(0.5f, Profile::scene());
//	CCDirector::sharedDirector()->replaceScene(transition);
    
    
    
    
    if(client->token.c_str() != NULL && strlen(client->token.c_str()) > 0){
        MockCommerceService* commerce = new MockCommerceService();
        void (*callback)(Commerce *commerce) = loadCommerceCallback;
        commerce->loadCommerce(client->token, "508b1c2ae4b04a375aa95176", callback);
        
        /*MockTransferService* transfer = new MockTransferService();
        void (*transcallback)() = transferCallback;
        transfer->transfer(client->token, transcallback);
        */
    }
    
    
}



void Main::menuSocialGameCallback(cocos2d::CCObject *pSender)
{
    //CCTransitionSlideInR* transition = CCTransitionSlideInR::create(0.5f, SocialGame::scene());
    CCTransitionSlideInR* transition = CCTransitionSlideInR::create(0.5f, LogrosScene::scene());
	CCDirector::sharedDirector()->replaceScene(transition);
}