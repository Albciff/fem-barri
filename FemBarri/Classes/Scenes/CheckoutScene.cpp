#include "CheckoutScene.h"
#include "SimpleAudioEngine.h"


#include "../NativeCallsEngine/include/NativeCalls.h"


using namespace cocos2d;
using namespace CocosDenshion;
using namespace NativeCallsEngine;


CCScene* Checkout::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();

    // 'layer' is an autorelease object
    Checkout *layer = Checkout::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Checkout::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }

    

    return true;
}

void Checkout::menuBackCallback(cocos2d::CCObject *pSender)
{

}