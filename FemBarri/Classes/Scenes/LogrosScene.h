//
//  LogrosScene.h
//  FemBarri
//
//  Created by Daniel García Pérez on 27/10/12.
//
//

#ifndef FemBarri_LogrosScene_h
#define FemBarri_LogrosScene_h

#include "cocos2d.h"

class LogrosScene : public cocos2d::CCLayer
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::CCScene* scene();
    
    // a selector callback
    void menuBackCallback(CCObject* pSender);
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(LogrosScene);
};

#endif
