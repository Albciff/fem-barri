//PrintMessage.h
#ifndef __PRINTMESSAGE_H__
#define __PRINTMESSAGE_H__

#include <stddef.h>


namespace NativeCallsEngine {
    
class NativeCalls
{
    
public:
    NativeCalls();
    ~NativeCalls();
    
    static NativeCalls* sharedEngine();
    
    static void end();
    
    void openURL(const char *url);
    void showMap();
    void removeMap();
    
    void login();
    void logout();
};
    
}

#endif//__PRINTMESSAGE_H__