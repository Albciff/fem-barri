#include "../include/NativeCalls.h"
#include <jni.h>
#include <android/log.h>
#include "../cocos2dx/platform/android/jni/JniHelper.h"

using namespace cocos2d;

static void static_end()
{

}

static void static_login()
{

}

static void static_logout()
{

}

static void static_showMap()
{
		JniMethodInfo methodInfo;
		if (! JniHelper::getStaticMethodInfo(methodInfo, "org/cocos2dx/simplegame/SimpleGame", "showMap", 
			"()V"))
		{
			return;
		}
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);

}

static void static_removeMap()
{

}




namespace NativeCallsEngine {
    
    static NativeCalls *s_pEngine;
    
    NativeCalls::NativeCalls()
    {
        
    }
    
    NativeCalls::~NativeCalls()
    {
        
    }
    
    NativeCalls* NativeCalls::sharedEngine()
    {
        if (! s_pEngine)
        {
            s_pEngine = new NativeCalls();
        }
        
        return s_pEngine;
    }
    
    void NativeCalls::end()
    {
        if (s_pEngine)
        {
            delete s_pEngine;
            s_pEngine = NULL;
        }
        
        static_end();
    }
    
    void NativeCalls::openURL(const char *url)
    {
        
    }
    
    void NativeCalls::login()
    {
        static_login();
    }
    
    void NativeCalls::logout()
    {
        static_logout();
    }

    void NativeCalls::showMap()
    {
        static_showMap();
    }

    void NativeCalls::removeMap()
    {
        static_removeMap();
    }
}
