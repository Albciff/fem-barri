#include "NativeCalls.h"
#include "NativeCalls_objc.h"

#import "EAGLView.h"

static void static_end()
{
    [NativeCalls end];
}

static void static_login()
{
    [NativeCalls login];
}

static void static_logout()
{
    [NativeCalls logout];
}

static void static_showMap()
{
    [NativeCalls mapView:YES];
}

static void static_removeMap()
{
    [NativeCalls mapView:NO];
}




namespace NativeCallsEngine {
    
    static NativeCalls *s_pEngine;
    
    NativeCalls::NativeCalls()
    {
        
    }
    
    NativeCalls::~NativeCalls()
    {
        
    }
    
    NativeCalls* NativeCalls::sharedEngine()
    {
        if (! s_pEngine)
        {
            s_pEngine = new NativeCalls();
        }
        
        return s_pEngine;
    }
    
    void NativeCalls::end()
    {
        if (s_pEngine)
        {
            delete s_pEngine;
            s_pEngine = NULL;
        }
        
        static_end();
    }
    
    void NativeCalls::openURL(const char *url)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithCString:url encoding:NSUTF8StringEncoding]]];
    }
    
    void NativeCalls::login()
    {
        static_login();
    }
    
    void NativeCalls::logout()
    {
        static_logout();
    }

    void NativeCalls::showMap()
    {
        static_showMap();
    }

    void NativeCalls::removeMap()
    {
        static_removeMap();
    }
}
