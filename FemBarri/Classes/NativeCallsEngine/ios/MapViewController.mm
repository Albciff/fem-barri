#import "MapViewController.h"
#import "cocos2d.h"
#import "EAGLView.h"

@implementation MapViewController

- (void) viewDidLoad
{
    [super viewDidLoad];

    cocos2d::CCSize size = cocos2d::CCDirector::sharedDirector()->getWinSize();
    mMap = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    mMap.delegate = self;

    [self.view addSubview:mMap];
    [[EAGLView sharedEGLView] addSubview:self.view];

    [[EAGLView sharedEGLView] sendSubviewToBack:self.view];

}

- (void)dealloc
{
    [super dealloc];
}


@end
