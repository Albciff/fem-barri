#import <Foundation/NSObject.h>
#import <GameKit/GameKit.h>
#import "EAGLView.h"

#import <UIKit/UIKit.h>


@interface NativeCalls : NSObject
{
    UIViewController *myViewController;
}

- (UIViewController*) getViewController;
- (void) setViewController: (UIViewController*) controller;

+ (NativeCalls*) sharedEngine;
+ (void) end;

+ (void) login;
+ (void) logout;

+ (void) mapView: (BOOL) visible;
@end

