#import "NativeCalls_objc.h" 
#import <stdio.h>
#import "AppController.h"

#include "MapViewController.h"

@implementation NativeCalls

static NativeCalls *sharedEngine = nil;

// Init
+ (NativeCalls *) sharedEngine
{
    @synchronized(self)
    {
        if (!sharedEngine)
            sharedEngine = [[NativeCalls alloc] init];
    }
    return sharedEngine;
}

+ (id) alloc
{
    @synchronized(self)
    {
        NSAssert(sharedEngine == nil, @"Attempted to allocate a second instance of a singleton.");
        return [super alloc];
    }
    return nil;
}

-(id) init
{
    if((self=[super init]))
    {
    }
    return self;
}

// Memory
- (void) dealloc
{
    [super dealloc];
}

- (UIViewController*) getViewController
{
    return myViewController;
}

- (void) setViewController: (UIViewController*) controller
{
    myViewController = controller;
}


+(void) end
{
    [sharedEngine release];
    sharedEngine = nil;
}

+ (void) login
{

}

+ (void) logout
{
    
}

+ (void) mapView: (BOOL)visible
{
    UIApplication* clientApp = [UIApplication sharedApplication];

    if (visible)
    {
        [((AppController*)clientApp.delegate) showMaps];
    }
    else
    {
        [((AppController*)clientApp.delegate) removeMaps];
    }


    /*
    if (visible)
    {
        MapViewController* controller = [[[MapViewController alloc] init] autorelease];

        if (controller != nil) {
            UIApplication* clientApp = [UIApplication sharedApplication];

            UIWindow* topWindow = [clientApp keyWindow];
            if (!topWindow)
            {
                topWindow = [[clientApp windows] objectAtIndex:0];
            }

            //controller.achievementDelegate = (AppController *)clientApp.delegate;
            //[[[topWindow rootViewController] view] addSubview:controller.view];
            //[[[topWindow rootViewController] view] sendSubviewToBack:controller.view];

            UIViewController* viewCon = (UIViewController*) ((AppController*)clientApp.delegate).viewController;
            [viewCon.view addSubview:controller.view];
        }
    }
    else
    {

    }*/
}

@end