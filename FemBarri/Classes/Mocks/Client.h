//
//  Client.h
//  FemBarri
//
//  Created by Daniel García Pérez on 26/10/12.
//
//

#ifndef FemBarri_Client_h
#define FemBarri_Client_h

#include <iostream>
#include "cocos2d.h"

using namespace std;

class Client : public cocos2d::CCObject
{
public:
    
    string id;
	string username;
    string password;
    string firstName;
    string lastName;
    
    string addressStreet;
    string addressNumber;
    string addressCity;
    string addressPostalCode;
    string addressCountry;
	
};

#endif
