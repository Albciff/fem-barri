#ifndef __PRODUCT_H__
#define __PRODUCT_H__

#include <iostream>

using namespace std;

typedef struct{

	string name;
	string id;
	string description;
	float price;
	string imagePath;
    
} Product;

#endif // __PRODUCT_H__