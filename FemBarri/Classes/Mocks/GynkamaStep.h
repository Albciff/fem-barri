#ifndef __GYNKAMASTEP_H__
#define __GYNKAMASTEP_H__

#include "cocos2d.h"
#include "ProductPromotion.h"

class GynkamaStep : public cocos2d::CCObject
{
public:

	GynkamaStep(ProductPromotion*,ProductPromotion*,ProductPromotion*);
	ProductPromotion* offer1;
	ProductPromotion* offer2;
	ProductPromotion* offer3;

	cocos2d::CCArray* getOffers();
};


#endif // __GYNKAMASTEP_H__