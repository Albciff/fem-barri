#include "./ProductPromotion.h"

using namespace std;

ProductPromotion::ProductPromotion(Product product, int idProductPromotion, int idCommerce, string description, Discount discount, string detailPath){

	this->product = product;
	this->idProductPromotion = idProductPromotion;
	this->idCommerce = idCommerce;
	this->description = description;
	this->discount = discount;
    this->detailPath = detailPath;
}