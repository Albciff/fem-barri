//
//  Commerce.h
//  FemBarri
//
//  Created by Daniel García Pérez on 27/10/12.
//
//

#ifndef FemBarri_Commerce_h
#define FemBarri_Commerce_h

#include <iostream>
#include "cocos2d.h"

using namespace std;

class Commerce : public cocos2d::CCObject
{
public:
    
    string id;
    
	string name;
    
    string addressStreet;
    string addressNumber;
    string addressCity;
    string addressPostalCode;
    string addressCountry;
    
    float latitud;
    float longitud;
	
};

#endif
