#ifndef __PRODUCTPROMOTION_H__
#define __PRODUCTPROMOTION_H__

#include <iostream>
#include "Product.h"
#include "cocos2d.h"

using namespace std;

enum discountType { ABS, PERCENTAGE};

typedef struct{
	enum discountType disc;
	float value;
} Discount;


class ProductPromotion : public cocos2d::CCObject
{
public:

	ProductPromotion(Product product, int idProductPromotion, int idCommerce, string description, Discount discount, string detailPath);

	Product product;
	int idProductPromotion;
	int idCommerce;
	string description;
	Discount discount;
    string detailPath;
};


#endif // __PRODUCTPROMOTION_H__