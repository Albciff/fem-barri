#include "./GynkamaStep.h"

using namespace std;
using namespace cocos2d;

GynkamaStep::GynkamaStep(ProductPromotion* product1,ProductPromotion* product2,ProductPromotion* product3){

	this->offer1 = product1;
	this->offer2 = product2;
	this->offer3 = product3;
}

CCArray* GynkamaStep::getOffers(){

	CCArray* offerArray = CCArray::create();
	offerArray->addObject(this->offer1);
	offerArray->addObject(this->offer2);
	offerArray->addObject(this->offer3);

	return offerArray;

}