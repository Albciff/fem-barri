#include "./MockProductService.h"

MockProductService::MockProductService(){
	
	// crear el array de productPromotion
	Product product = {"1","Taronjes fresques","Taronjes",0.8,"tira1-01.png"};

	Discount discount = { ABS, 1.0f};
	ProductPromotion* promotion = new ProductPromotion(product, 1, 1, "Taronjes oferta", discount, "oferta2.png");

	productPromotions = new CCDictionary();
	productPromotions->setObject(promotion,promotion->idProductPromotion);

	// 
	Product product2 = {"2","Flors de jard�","Flors de jard�",3.5,"tira9-01.png"};
	promotion = new ProductPromotion(product2, 2, 1, "Flors oferta", discount, "oferta2.png"); 
	productPromotions->setObject(promotion,promotion->idProductPromotion);

	//
	Product product3 = {"3","Pastissos manuals","Pastissos manuals",4.3,"tira2-01.png"};
	promotion = new ProductPromotion(product3, 3, 1, "Pastissos oferta", discount, "oferta2.png"); 
    
    productPromotions->setObject(promotion,promotion->idProductPromotion);

    Product product4 = {"4","Llibre","Llibre",14.5,"tira3-01.png"};
	promotion = new ProductPromotion(product4, 4, 1, "Llibre oferta", discount, "oferta3.png");
    
    productPromotions->setObject(promotion,promotion->idProductPromotion);
    
    Product product5 = {"5","Sabates","Sabates",14.5,"tira5-01.png"};
	promotion = new ProductPromotion(product5, 5, 1, "Sabates oferta", discount, "oferta3.png");
    
	productPromotions->setObject(promotion,promotion->idProductPromotion);
    
    Product product6 = {"6","Cinema","Cinema",8.0,"tira4-01.png"};
	promotion = new ProductPromotion(product6, 6, 1, "Cinema oferta", discount, "oferta3.png");
    
	productPromotions->setObject(promotion,promotion->idProductPromotion);
    
    Product product7 = {"7","Pilota","Pilota",14.5,"tira6-01.png"};
	promotion = new ProductPromotion(product7, 7, 1, "Pilota oferta", discount, "oferta1.png");
    
	productPromotions->setObject(promotion,promotion->idProductPromotion);
    
    Product product8 = {"8","Ous","Ous",1.35,"tira7-01.png"};
	promotion = new ProductPromotion(product8, 8, 1, "Ous oferta", discount, "oferta1.png");
    
	productPromotions->setObject(promotion,promotion->idProductPromotion);
    
    Product product9 = {"9","Barret","Barret",12.72,"tira8-01.png"};
	promotion = new ProductPromotion(product9, 9, 1, "Barret oferta", discount, "oferta1.png");
    
	productPromotions->setObject(promotion,promotion->idProductPromotion);
}

MockProductService::~MockProductService(){
	// destroy da fucking array el array de productPromotion
}


ProductPromotion* MockProductService::getProductPromotion(int idPP){
	return (ProductPromotion*)productPromotions->objectForKey(idPP);
}

GynkamaStep* MockProductService::getGynkamaStep(int step){

	int count = step * 3 + 1;
	GynkamaStep* gStep = new GynkamaStep(getProductPromotion(count),getProductPromotion(count+1),getProductPromotion(count+2));
	return gStep;
}


