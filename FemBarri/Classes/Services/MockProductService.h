#ifndef __MOCKPRODUCTSERVICES_H__
#define __MOCKPRODUCTSERVICES_H__

#include <iostream>
#include "cocos2d.h"
#include "../Mocks/ProductPromotion.h"
#include "../Mocks/GynkamaStep.h"

using namespace std;
using namespace cocos2d;

class MockProductService
{
public:
	MockProductService();
	~MockProductService();
	ProductPromotion* getProductPromotion(int idPP);
	GynkamaStep* getGynkamaStep(int step);

private:

	CCDictionary* productPromotions;


};


#endif // __MOCKPRODUCTSERVICES_H__