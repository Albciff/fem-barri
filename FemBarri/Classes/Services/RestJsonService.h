#ifndef __JSONSERVICE_H__
#define __JSONSERVICE_H__

#include "cocos2d.h"
#include "../extensions/cocos-ext.h"

class RestJsonService : public cocos2d::CCObject
{
public:

	RestJsonService();
	virtual ~RestJsonService();

	void login();

};


#endif // __JSONSERVICE_H__