//
//  MockClientService.h
//  FemBarri
//
//  Created by Daniel García Pérez on 26/10/12.
//
//

#ifndef FemBarri_MockClientService_h
#define FemBarri_MockClientService_h

#include <iostream>
#include "cocos2d.h"
#include "../Mocks/Client.h"
#include "../extensions/cocos-ext.h"

using namespace std;
using namespace cocos2d;

class MockClientService: public cocos2d::CCObject
{
public:
	MockClientService();
	~MockClientService();
	void login(string username, string password);
    void loadClientProfile(string token);
	void onHttpRequestCompleted(CCObject *pSender, void *data);
    void onHttpRequestLoadClientCompleted(CCObject *pSender, void *data);
    
    string token;
    Client *client;
private:
    
    
};

#endif
