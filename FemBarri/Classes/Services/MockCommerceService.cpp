//
//  MockCommerceService.cpp
//  FemBarri
//
//  Created by Daniel García Pérez on 27/10/12.
//
//

#include "MockCommerceService.h"
#include "../Constants.h"
#include "../json/json.h"

USING_NS_CC;
USING_NS_CC_EXT;

MockCommerceService::MockCommerceService(){}

MockCommerceService::~MockCommerceService(){}

/*
void MockCommerceService::login(string username, string password){
    CCHttpRequest* request = new CCHttpRequest();
    request->setUrl("http://finappsapi.bdigital.org/api/2012/33e34fbe0f/access/login");
    request->setRequestType(CCHttpRequest::kHttpGet);
    request->setHeaders("Authorization: Basic Qm90aWdhMzpCb3RpZ2Ez");
    request->setResponseCallback(this, callfuncND_selector(MockCommerceService::onHttpRequestCompleted));
    CCHttpClient::getInstance()->send(request);
    request->release();
    
    
}*/

void MockCommerceService::loadCommerce(string token, string idCommerce, void (*functocall)(Commerce *comerce)){
    CCHttpRequest* request = new CCHttpRequest();
    CCString* url = CCString::createWithFormat("%s%s%s%s","http://finappsapi.bdigital.org/api/2012/33e34fbe0f/",token.c_str(),"/operations/commerce/", idCommerce.c_str());
    request->setUrl(url->getCString());
    request->setRequestType(CCHttpRequest::kHttpGet);
    request->setResponseCallback(this, callfuncND_selector(MockCommerceService::onHttpRequestLoadCommerceCompleted));
    this->callback = functocall;
    CCHttpClient::getInstance()->send(request);
    request->release();
}
/*
void MockCommerceService::onHttpRequestCompleted(cocos2d::CCObject *pSender, void *data)
{
    CCLog("login Data: %s", ((CCHttpResponse *)data)->getResponseData()->data());
    
    string response = ((CCHttpResponse *)data)->getResponseData()->data();
    
    Json::Value root;
    Json::Reader reader;
    bool parsedSuccess = reader.parse(response,
                                      root,
                                      false);
    
    if(not parsedSuccess)
    {
        // Report failures and their locations
        // in the document.
        cout<<"Failed to parse Login JSON"<<endl
        <<reader.getFormatedErrorMessages()
        <<endl;
        
    }
    
    const Json::Value tokenJSON = root["token"];
    
    
    token = tokenJSON.asString();
    
    CCLog("Token of commerce: %s", token.c_str());
    
    MockCommerceService::loadCommerce(token);
    
}*/

void MockCommerceService::onHttpRequestLoadCommerceCompleted(CCObject *pSender, void *data){
    commerce  = new Commerce();
    
    CCLog("Commerce Data: %s", ((CCHttpResponse *)data)->getResponseData()->data());
    
    string response = ((CCHttpResponse *)data)->getResponseData()->data();
    
    Json::Value root;
    Json::Reader reader;
    bool parsedSuccess = reader.parse(response,
                                      root,
                                      false);
    
    if(not parsedSuccess)
    {
        // Report failures and their locations
        // in the document.
        cout<<"Failed to parse Commerce JSON"<<endl
        <<reader.getFormatedErrorMessages()
        <<endl;
        
    }
    
    const Json::Value dataJSON = root["data"];
    
    
    commerce->name=dataJSON["publicName"].asString();
    
    const Json::Value addressJSON = dataJSON["address"];
    
    commerce->addressStreet=addressJSON["street"].asString();
    commerce->addressNumber=addressJSON["number"].asString();
    commerce->addressPostalCode=addressJSON["postalCode"].asString();
    commerce->addressCity=addressJSON["city"].asString();
    commerce->addressCountry=addressJSON["country"].asString();
    
    CCLog("Commerce Data Load:  name:%s  street:%s number:%s postalCode:%s city:%s country:%s",  commerce->name.c_str(), commerce->addressStreet.c_str(), commerce->addressNumber.c_str(), commerce->addressPostalCode.c_str(), commerce->addressCity.c_str(), commerce->addressCountry.c_str());
    
    (*this->callback)(this->commerce);
}