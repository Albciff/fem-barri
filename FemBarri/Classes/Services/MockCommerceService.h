//
//  MockCommerceService.h
//  FemBarri
//
//  Created by Daniel García Pérez on 27/10/12.
//
//

#ifndef FemBarri_MockCommerceService_h
#define FemBarri_MockCommerceService_h

#include <iostream>
#include "cocos2d.h"
#include "../Mocks/Commerce.h"
#include "../extensions/cocos-ext.h"

using namespace std;
using namespace cocos2d;

class MockCommerceService: public cocos2d::CCObject
{
public:
	MockCommerceService();
	~MockCommerceService();
    //void login(string username, string password);
    void loadCommerce(string token, string idCommerce, void (*functocall)(Commerce *comerce));
    //void onHttpRequestCompleted(CCObject *pSender, void *data);
    void onHttpRequestLoadCommerceCompleted(CCObject *pSender, void *data);
    
    string token;
    Commerce *commerce;
    void (*callback)(Commerce *comerce);
private:
    
    
};

#endif
