//
//  MockClientService.h
//  FemBarri
//
//  Created by Daniel García Pérez on 26/10/12.
//
//

#ifndef FemBarri_MockTransferService_h
#define FemBarri_MockTransferService_h

#include <iostream>
#include "cocos2d.h"
#include "../Mocks/Client.h"
#include "../extensions/cocos-ext.h"

using namespace std;
using namespace cocos2d;

class MockTransferService: public cocos2d::CCObject
{
public:
	MockTransferService();
	~MockTransferService();
	void transfer(string token, void (*functocall)());
	void onHttpRequestTransferedComplete(CCObject *pSender, void *data);
    
    void (*callback)();
private:
    
    
};

#endif
