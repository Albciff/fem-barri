//
//  MockClientService.cpp
//  FemBarri
//
//  Created by Daniel García Pérez on 26/10/12.
//
//

#include "MockClientService.h"
#include "../Constants.h"
#include "../json/json.h"

USING_NS_CC;
USING_NS_CC_EXT;



MockClientService::MockClientService(){}

MockClientService::~MockClientService(){}

void MockClientService::login(string username, string password){
    CCHttpRequest* request = new CCHttpRequest();
    request->setUrl("http://finappsapi.bdigital.org/api/2012/33e34fbe0f/access/login");
    request->setRequestType(CCHttpRequest::kHttpGet);
    request->setHeaders("Authorization: Basic QWxiZXJ0Ok9zY2Fy");
    request->setResponseCallback(this, callfuncND_selector(MockClientService::onHttpRequestCompleted));
    CCHttpClient::getInstance()->send(request);
    request->release();
    
    
}



void MockClientService::loadClientProfile(string token){
    
    CCHttpRequest* request = new CCHttpRequest();
    CCString* url = CCString::createWithFormat("%s%s%s","http://finappsapi.bdigital.org/api/2012/33e34fbe0f/",token.c_str(),"/operations/client/profile/@me");
    request->setUrl(url->getCString());
    request->setRequestType(CCHttpRequest::kHttpGet);
    request->setResponseCallback(this, callfuncND_selector(MockClientService::onHttpRequestLoadClientCompleted));
    CCHttpClient::getInstance()->send(request);
    request->release();
    
    
}

void MockClientService::onHttpRequestCompleted(cocos2d::CCObject *pSender, void *data)
{
    CCLog("login Data: %s", ((CCHttpResponse *)data)->getResponseData()->data());
    
    string response = ((CCHttpResponse *)data)->getResponseData()->data();
    
    Json::Value root;
    Json::Reader reader;
    bool parsedSuccess = reader.parse(response,
                                      root,
                                      false);
    
    if(not parsedSuccess)
    {
        // Report failures and their locations
        // in the document.
        cout<<"Failed to parse Login JSON"<<endl
        <<reader.getFormatedErrorMessages()
        <<endl;
        
    }
    
    const Json::Value tokenJSON = root["token"];
    
    
    token = tokenJSON.asString();
    
    CCLog("Token of user: %s", token.c_str());
    
    MockClientService::loadClientProfile(token);
    
}

void MockClientService::onHttpRequestLoadClientCompleted(cocos2d::CCObject *pSender, void *data)
{
    client = new Client();
    
    //CCLog("Client Data: %s", ((CCHttpResponse *)data)->getResponseData()->data());
    
    string response = ((CCHttpResponse *)data)->getResponseData()->data();
    
    Json::Value root;
    Json::Reader reader;
    bool parsedSuccess = reader.parse(response,
                                      root,
                                      false);
    
    if(not parsedSuccess)
    {
        // Report failures and their locations
        // in the document.
        cout<<"Failed to parse Client JSON"<<endl
        <<reader.getFormatedErrorMessages()
        <<endl;
        
    }
    
    const Json::Value dataJSON = root["data"];
    
    client->id = dataJSON["id"].asString();
    
    const Json::Value holderJSON = dataJSON["holder"];
    
    client->username=holderJSON["username"].asString();
    client->password=holderJSON["password"].asString();
    client->firstName=holderJSON["firstName"].asString();
    client->lastName=holderJSON["lastName"].asString();
    
    const Json::Value addressJSON = holderJSON["address"];
    
    client->addressStreet=addressJSON["street"].asString();
    client->addressNumber=addressJSON["number"].asString();
    client->addressPostalCode=addressJSON["postalCode"].asString();
    client->addressCity=addressJSON["city"].asString();
    client->addressCountry=addressJSON["country"].asString();
    
    CCLog("Client Data Load: id:%s username:%s password:%s firstName:%s lastName:%s street:%s number:%s postalCode:%s city:%s country:%s", client->id.c_str(), client->username.c_str(), client->password.c_str(), client->firstName.c_str(), client->lastName.c_str(), client->addressStreet.c_str(), client->addressNumber.c_str(), client->addressPostalCode.c_str(), client->addressCity.c_str(), client->addressCountry.c_str());
}