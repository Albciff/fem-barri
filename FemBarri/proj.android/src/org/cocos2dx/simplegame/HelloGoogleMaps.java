package org.cocos2dx.simplegame;

import java.util.List;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class HelloGoogleMaps extends MapActivity {
	
	@Override
	protected boolean isRouteDisplayed() {
	    return false;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.main);
	    
	    MapView mapView = (MapView) findViewById(R.id.mapview);
	    mapView.setBuiltInZoomControls(true);
	    	    
	    List<Overlay> mapOverlays = mapView.getOverlays();
	    Drawable drawable = this.getResources().getDrawable(R.drawable.androidmarker);
	    HelloItemizedOverlay itemizedoverlay = new HelloItemizedOverlay(drawable, this);
	    
	    GeoPoint point = new GeoPoint(41397351,2183827);
	    OverlayItem overlayitem = new OverlayItem(point, "Alimentació paquito", "Embutits");
	    
	    itemizedoverlay.addOverlay(overlayitem);
	    mapOverlays.add(itemizedoverlay);
	    
	    point = new GeoPoint(41407351,2184825);
	    overlayitem = new OverlayItem(point, "Zapateria pepe", "Sabates");
	    
	    itemizedoverlay.addOverlay(overlayitem);
	    mapOverlays.add(itemizedoverlay);
	    
	    point = new GeoPoint(41397737,2186322);
	    overlayitem = new OverlayItem(point, "Sexshop Oscar", "Vibradors i mes");
	    
	    itemizedoverlay.addOverlay(overlayitem);
	    mapOverlays.add(itemizedoverlay);
	    
	    point = new GeoPoint(41400924,2199154);
	    overlayitem = new OverlayItem(point, "Artesania Pep", "Textil");
	    
	    itemizedoverlay.addOverlay(overlayitem);
	    mapOverlays.add(itemizedoverlay);
	    	    
	    point = new GeoPoint(41399942,2197073);
	    overlayitem = new OverlayItem(point, "BDDigital", "Barcelona TIC");
	    
	    itemizedoverlay.addOverlay(overlayitem);
	    mapOverlays.add(itemizedoverlay);
	    	
	    point = new GeoPoint(41399685,2186944);
	    overlayitem = new OverlayItem(point, "Fleca el clot", "Fleca");
	    
	    itemizedoverlay.addOverlay(overlayitem);
	    mapOverlays.add(itemizedoverlay);
	    
	    
	    
	    
	    
	}

}
