LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := game_shared

LOCAL_MODULE_FILENAME := libgame

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/Mocks/GynkamaStep.cpp \
                   ../../Classes/Mocks/ProductPromotion.cpp \
                   ../../Classes/NativeCallsEngine/android/NativeCalls.cpp \
                   ../../Classes/Scenes/CheckoutScene.cpp \
                   ../../Classes/Scenes/GameScene.cpp \
                   ../../Classes/Scenes/GameModeScene.cpp \
                   ../../Classes/Scenes/MainScene.cpp \
                   ../../Classes/Scenes/OfferDetailsScene.cpp \
                   ../../Classes/Scenes/OffersScene.cpp \
                   ../../Classes/Scenes/SocialGameScene.cpp \
                   ../../Classes/Scenes/SplashScene.cpp \
                   ../../Classes/Scenes/LogrosScene.cpp \
                   ../../Classes/Scenes/GameProgressScene.cpp \
                   ../../Classes/Services/MockClientService.cpp \
                   ../../Classes/Services/MockProductService.cpp \
                   ../../Classes/Services/RestJsonService.cpp \
                   ../../Classes/Services/MockCommerceService.cpp \
                   ../../Classes/Services/MockTransferService.cpp \
                   ../../Classes/Test/Test.cpp \
                   ../../Classes/lib_json/json_reader.cpp \
                   ../../Classes/lib_json/json_value.cpp \
                   ../../Classes/lib_json/json_writer.cpp

                                      
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes                   

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static cocosdenshion_static cocos_extension_static
            
include $(BUILD_SHARED_LIBRARY)

$(call import-module,CocosDenshion/android) \
$(call import-module,cocos2dx) \
$(call import-module,extensions)
